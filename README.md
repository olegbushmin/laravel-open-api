# Open Api Scheme Generator

## Description
This library is used to generate an open api scheme based on reflection of your code

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

``php artisan vendor:publish --provider="OpenApi\OpenApiServiceProvider"``


## Usage
``php artisan oa:generate``

