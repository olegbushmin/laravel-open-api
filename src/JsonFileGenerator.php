<?php

namespace OpenApi;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Config;
use OpenApi\Enums\HttpMethodEnum;
use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Properties\OAObjectProperty;
use OpenApi\Json\Properties\OAPaginationInfoProperty;
use OpenApi\Objects\ParsedRoute;

class JsonFileGenerator
{
    /**
     * @var array<ParsedRoute>
     */
    private array $parsedRoutes;

    private array $jsonData = [];

    public function __construct(array $parsedRoutes)
    {
        $this->parsedRoutes = $parsedRoutes;
    }

    public function generate(): void
    {
        $this->jsonData = [];

        $paths = [];
        foreach ($this->parsedRoutes as $key => $parsedRoute) {
            $uri = "/$parsedRoute->uri";

            if (!array_key_exists($uri, $paths)) {
                $paths[$uri] = [];
            }

            $method = strtolower($parsedRoute->methods[0]);

            $parameters = [];

            if ($parsedRoute->summary) {
                $paths[$uri][$method]['summary'] = $parsedRoute->summary;
            }

            if ($parsedRoute->pathParameters) {
                foreach ($parsedRoute->pathParameters as $parameter) {
                    $parameters[] = [
                        'name'     => trim($parameter, '?'),
                        'required' => !str_ends_with($parameter, '?'),
                        'in'       => 'path',
                        'schema'   => (object) [],
                    ];
                }
            }

            if ($parsedRoute->requestScheme) {
                if (\Str::upper($method) !== HttpMethodEnum::Get->value) {
                    $paths[$uri][$method]['requestBody'] = $this->defineRequestBody($parsedRoute);
                } else {
                    if ($parsedRoute->requestScheme->oneOf) {
                        throw new \RuntimeException("Route '$parsedRoute->uri': this for GET operations not implemented yet");
                    }

                    foreach ($parsedRoute->requestScheme->properties as $fieldName => $property) {
                        $parameters[] = [
                            'name'     => $fieldName,
                            'required' => $property->required,
                            'in'       => 'query',
                            'schema'   => $property->toArray(),
                        ];
                    }
                }
            }

            if ($parameters) {
                $paths[$uri][$method]['parameters'] = $parameters;
            }

            $isDeleteMethod = \Str::upper($method) === HttpMethodEnum::Delete->value;

            $paths[$uri][$method]['responses'] = (object) $this->defineResponses($parsedRoute, $isDeleteMethod);

            if (in_array("auth:api", $parsedRoute->middlewares)) {
                $paths[$uri][$method]['security'][] = [
                    'bearerAuth' => [],
                ];
            }
        }

        $this->jsonData = [
            'openapi'    => '3.0.0',
            'info'       => [
                'title'   => Config::get('open-api.title'),
                'version' => '0.1',
            ],
            'components' => [
                'securitySchemes' => [
                    'bearerAuth' => [
                        'type'         => 'http',
                        'scheme'       => 'bearer',
                        'bearerFormat' => 'JWT',
                    ],
                ],
            ],
            'paths'      => $paths,
        ];
    }

    private function defineRequestBody(ParsedRoute $parsedRoute): array
    {
        return [
            'required' => (bool) $parsedRoute->requestScheme,
            'content'  => [
                'application/json' => [
                    'schema' => (object) ($parsedRoute->requestScheme?->toArray() ?? []),
                ],
            ],
        ];
    }

    private function defineResponses(ParsedRoute $parsedRoute, bool $isDeleteMethod): array
    {
        $responses = [];

        foreach ($parsedRoute->responses ?: [200 => null] as $statusCode => $schema) {

            $responses[$statusCode] = [
                'description' => 'description',
            ];

            if (!$schema) {
                $schemaJson = (object) [];
            } else {
                $baseResource = config("open-api.base_resource");
                $wrap = $baseResource::$wrap;

                $wrapProperty           = new OAObjectProperty();
                $wrapProperty->required = true;

                $wrapProperty->properties         = [
                    $wrap => (object) $schema->toArray(),
                ];
                $wrapProperty->requiredProperties = [
                    $wrap,
                ];

                if ($schema instanceof OAArrayProperty && $schema->isPagination) {
                    $wrapProperty->properties['meta']   = (new OAPaginationInfoProperty())->toArray();
                    $wrapProperty->requiredProperties[] = 'meta';
                }

                $schemaJson = $wrapProperty->toArray();
            }

            if (!$isDeleteMethod) {
                $responses[$statusCode]['content'] = [
                    'application/json' => [
                        'schema' => $schemaJson,
                    ],
                ];
            }
        }

        return $responses;
    }

    public function saveToDisk(Filesystem $filesystem, string $fileName)
    {
        $json = json_encode($this->jsonData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        $filesystem->put("$fileName.json", $json);
    }
}
