<?php

namespace OpenApi;

use Illuminate\Routing\Route;
use OpenApi\Objects\ParsedRoute;

class RoutersParser
{
    /**
     * @param array<Route> $routes
     *
     * @throws \ReflectionException
     * @return array<ParsedRoute>
     */
    public function parseRoutes(array $routes): array
    {
        $parsedRoutes = [];

        foreach ($routes as $route) {
            if ($route->getActionName() === 'Closure') {
                continue;
            }

            $parser = new RouterParser($route);

            $parsedRoutes[] = $parser->parse();
        }

        return $parsedRoutes;
    }

}
