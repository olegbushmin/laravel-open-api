<?php

namespace OpenApi\Attributes;

class OAAttributeHelper
{
    public static function getListItemTypeAttribute(
        \ReflectionMethod|\ReflectionProperty $reflection
    ): ?OAListItemTypeAttribute {
        return self::getInstanceByReflection($reflection, OAListItemTypeAttribute::class);
    }

    public static function getSummaryAttribute(
        \ReflectionMethod|\ReflectionProperty $reflection
    ): ?OASummaryAttribute {
        return self::getInstanceByReflection($reflection, OASummaryAttribute::class);
    }

    public static function getInstanceByReflection(
        \ReflectionMethod|\ReflectionProperty $reflection,
        string $attributeClass
    ): mixed {
        return ($reflection->getAttributes($attributeClass)[0] ?? null)?->newInstance();
    }
}