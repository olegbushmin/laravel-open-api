<?php

namespace OpenApi\Attributes;
use Attribute;

#[Attribute]
class OASummaryAttribute
{
    public function __construct(private readonly string $langKey)
    {
    }

    public function get(): string
    {
        return __($this->langKey);
    }
}
