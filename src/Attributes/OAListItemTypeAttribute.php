<?php

namespace OpenApi\Attributes;

use Attribute;

#[Attribute]
class OAListItemTypeAttribute
{
    public function __construct(private readonly string $className, private readonly bool $isPagination = false)
    {
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function isPagination(): bool
    {
        return $this->isPagination;
    }
}
