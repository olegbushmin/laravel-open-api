<?php

namespace OpenApi;

use Illuminate\Support\Facades\Facade;

class OpenApiFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return OpenApiService::class;
    }
}