<?php

namespace OpenApi;

use Illuminate\Support\Facades\Storage;

class OpenApiService
{
    private static array $pathPrefixes = [
        'common' => [
            'api',
        ],
    ];

    /**
     * @throws \ReflectionException
     */
    public static function generateScheme(): void
    {
        $parser = new RoutersParser();

        foreach (self::$pathPrefixes as $name => $pathPrefixes) {
            $parsedRoutes = $parser->parseRoutes(
                (new RouterService($pathPrefixes))->getRoutes()
            );

            $jsonGenerator = new JsonFileGenerator($parsedRoutes);

            $jsonGenerator->generate();

            if (!\App::runningUnitTests()) {
                $disk = 'api-json-schemes';

                $jsonGenerator->saveToDisk(
                    Storage::disk($disk),
                    "$name-openapi-scheme"
                );
            }
        }
    }
}
