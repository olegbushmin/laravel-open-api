<?php

namespace OpenApi;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use OpenApi\Attributes\OAAttributeHelper;
use OpenApi\Attributes\OAListItemTypeAttribute;
use OpenApi\Converters\Request\OARulesParser;
use OpenApi\Converters\Responses\OAResponsesParser;
use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Properties\OANumberProperty;
use OpenApi\Json\Schemes\OAScheme;
use OpenApi\Objects\ParsedRoute;
use ReflectionClass;

class RouterParser
{
    private string $requestParentClass = FormRequest::class;
    public static string $resourcesParentClass = JsonResource::class;
    private \ReflectionMethod $reflectionMethod;
    private string $methodView;
    private ?OAListItemTypeAttribute $listItemTypeAttribute;

    public function __construct(private readonly Route $route)
    {
        $controllerReflection = new ReflectionClass($this->route->getController());

        $this->reflectionMethod = $controllerReflection->getMethod(
            Str::parseCallback($this->route->getAction('uses'))[1]
        );

        $this->methodView = $this->reflectionMethod->getDeclaringClass()->getName() . '::' . $this->reflectionMethod->getName();

        $this->listItemTypeAttribute = OAAttributeHelper::getListItemTypeAttribute($this->reflectionMethod);
    }

    public function parse(): ParsedRoute
    {
        $parsedRoute = new ParsedRoute();

        $parsedRoute->summary        = OAAttributeHelper::getSummaryAttribute($this->reflectionMethod)?->get();
        $parsedRoute->uri            = str_replace('?}', '}', $this->route->uri());
        $parsedRoute->methods        = $this->route->methods();
        $parsedRoute->middlewares    = array_diff($this->route->middleware(), $this->route->excludedMiddleware());
        $parsedRoute->pathParameters = $this->getPathParameters();
        $parsedRoute->requestScheme  = $this->getRequestScheme();
        $parsedRoute->responses      = $this->getResponsesFromProperties();

        if ($this->listItemTypeAttribute) {
            if (count($parsedRoute->responses) !== 1
                || ! array_values($parsedRoute->responses)[0] instanceof OAArrayProperty
            ) {
                throw new \RuntimeException("Invalid attribute usage for $this->methodView");
            }
        }

        return $parsedRoute;
    }


    private function getRequestScheme(): ?OAScheme
    {
        $result = null;

        foreach ($this->reflectionMethod->getParameters() as $parameter) {
            if (! $parameterType = $parameter->getType()?->getName()) {
                continue;
            }

            if (!class_exists($parameterType)
                || !is_subclass_of($parameterType, $this->requestParentClass)
            ) {
                continue;
            }

            $requestInstance = new $parameterType();

            $result = (new OARulesParser($requestInstance))->getScheme();

            if ($this->listItemTypeAttribute?->isPagination()) {
                $pageProperty = new OANumberProperty();
                $pageProperty->required = true;

                $result->properties['page'] = $pageProperty;
            }

            break;
        }

        return $result;
    }

    private function getResponsesFromProperties(): array
    {
        $returnTypes = [];

        $rcReturnType = $this->reflectionMethod->getReturnType();

        if ($rcReturnType instanceof \ReflectionNamedType) {
            $returnTypes = [$rcReturnType->getName()];
        } elseif ($rcReturnType instanceof \ReflectionUnionType) {
            $returnTypes = array_map(function (\ReflectionNamedType $type) {
                return $type->getName();
            }, $rcReturnType->getTypes());
        }

        if (!$returnTypes) {
            return [];
        }

        $schemes = [];

        $isList       = false;
        $isPagination = false;

        foreach ($returnTypes as $returnType) {
            if (!$this->classIsResource($returnType)) {
                continue;
            }

            if ($returnType === AnonymousResourceCollection::class) {
                if (count($returnTypes) !== 1) {
                    throw new \RuntimeException(
                        "Method: $this->methodView" . PHP_EOL .
                        "For such method should be one return type: AnonymousResourceCollection"
                    );
                }

                $returnType = $this->listItemTypeAttribute?->getClassName();

                $returnType = trim($returnType, '\\');

                if (!$returnType || !$this->classIsResource($returnType)) {
                    throw new \RuntimeException(
                        "Method: $this->methodView" . PHP_EOL .
                        "Not found attribute: " . class_basename(OAListItemTypeAttribute::class)
                    );
                }

                $isList       = true;
                $isPagination = $this->listItemTypeAttribute->isPagination();
            }

            if (!array_key_exists(200, $schemes)) {
                $schemes[200] = new OAScheme();
            }

            $schema = (new OAResponsesParser($returnType))->getScheme();

            if (!$isList) {
                $schemes[200]->oneOf[] = $schema;
            } else {
                $arrayProperty               = new OAArrayProperty();
                $arrayProperty->items        = $schema;
                $arrayProperty->isPagination = $isPagination;

                $schemes[200]->oneOf[] = $arrayProperty;
            }
        }

        foreach ($schemes as $key => $value) {
            if (count($value->oneOf) === 1) {
                $schemes[$key] = $value->oneOf[0];
            }
        }

        return $schemes;
    }

    public static function classIsResource(string $class): bool
    {
        return class_exists($class) && is_subclass_of($class, self::$resourcesParentClass);
    }

    private function getPathParameters(): array
    {
        preg_match_all('/\{(.*?)\}/', $this->route->uri(), $matches);

        return $matches[1] ?? [];
    }
}
