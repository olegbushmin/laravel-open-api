<?php

namespace OpenApi\Objects;

use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Schemes\OAScheme;

class ParsedRoute
{
    public ?string $summary = null;

    public string $uri;

    /** @var array<string> */
    public array $methods;
    /** @var array<string> */
    public array $middlewares;

    /** @var array<string> */
    public array $pathParameters;

    public ?OAScheme $requestScheme;

    /** @var array<int, OAScheme|OAArrayProperty> */
    public array $responses;
}
