<?php

namespace OpenApi\Enums;

use Illuminate\Validation\Rules\Enum;

class EnumRule extends Enum
{
    public function getType(): string
    {
        return $this->type;
    }
}
