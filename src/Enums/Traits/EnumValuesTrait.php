<?php

namespace OpenApi\Enums\Traits;

trait EnumValuesTrait
{
    public static function values(): array
    {
        return array_map(function (self $enum) {
            return $enum->value;
        }, static::cases());
    }
}
