<?php

namespace OpenApi\Enums;

use OpenApi\Enums\Traits\EnumValuesTrait;

enum HttpMethodEnum: string
{
    use EnumValuesTrait;

    case Get = 'GET';
    case Post = 'POST';
    case Delete = 'DELETE';
    case Put = 'PUT';
    case Patch = 'PATCH';
}

