<?php

namespace OpenApi\Console\Commands;

use Illuminate\Console\Command;
use OpenApi\OpenApiService;

class OpenApiGenerateCommand extends Command
{
    protected $signature = 'oa:generate';

    protected $description = 'Generate open-api json schemes';

    public function handle(): int
    {
        OpenApiService::generateScheme();

        $this->info('Json schemes was generated!');

        return 0;
    }
}