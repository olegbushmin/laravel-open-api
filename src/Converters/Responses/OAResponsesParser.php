<?php

namespace OpenApi\Converters\Responses;

use OpenApi\Attributes\OAAttributeHelper;
use OpenApi\Attributes\OAListItemTypeAttribute;
use OpenApi\Json\BaseOAJsonObject;
use OpenApi\Json\Properties\BaseOAProperty;
use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Properties\OABooleanProperty;
use OpenApi\Json\Properties\OAEnumProperty;
use OpenApi\Json\Properties\OAIntegerProperty;
use OpenApi\Json\Properties\OANumberProperty;
use OpenApi\Json\Properties\OAStringProperty;
use OpenApi\Json\Schemes\OAScheme;
use OpenApi\RouterParser;

class OAResponsesParser
{
    public function __construct(private readonly string $resourceClass)
    {
    }

    public function getScheme(): BaseOAJsonObject
    {
        $resourceReflection = new \ReflectionClass($this->resourceClass);

        $scheme = new OAScheme();

        foreach ($resourceReflection->getProperties() as $reflectionProperty) {
            if (!$reflectionProperty->isProtected()
                || $reflectionProperty->getDeclaringClass()->getName() != $this->resourceClass
            ) {
                continue;
            }

            $scheme->properties[$reflectionProperty->getName()] = $this->parseReflectionProperty($reflectionProperty);
        }

        if (!$scheme->properties) {
            throw new \RuntimeException('Not found protected properties for ' . $this->resourceClass);
        }

        foreach ($scheme->properties as $fieldName => $property) {
            if ($property->required) {
                $scheme->required[] = $fieldName;
            }
        }

        return $scheme;
    }

    private function parseReflectionProperty(\ReflectionProperty $reflectionProperty): BaseOAProperty|OAScheme
    {
        $propertyType = $reflectionProperty->getType();

        if (method_exists($propertyType, 'getTypes')) {
            $propertyType = $propertyType->getTypes()[0];
        }

        $propertyTypeName = $propertyType->getName();

        if ($propertyTypeName !== 'array') {
            $property = $this->detectJsonProperty($propertyTypeName);
        } else {
            $property = new OAArrayProperty();

            $propertyTypeName = OAAttributeHelper::getListItemTypeAttribute($reflectionProperty)?->getClassName();

            $propertyView = $reflectionProperty->getDeclaringClass()->getName() .
                '::' . $reflectionProperty->getName();

            if (!$propertyTypeName) {
                throw new \RuntimeException(
                    "Method: $propertyView" . PHP_EOL .
                    "Not found attribute: " . class_basename(OAListItemTypeAttribute::class)
                );
            }

            $propertyTypeName = trim($propertyTypeName, '\\');

            $property->items = $this->detectJsonProperty($propertyTypeName);
        }

        $property->nullable = $propertyType->allowsNull();

        if (! $property instanceof OAScheme) {
            $property->required = !$property->nullable;
        }

        return $property;
    }

    private function detectJsonProperty(string $propertyTypeName): BaseOAJsonObject
    {
        if ($propertyTypeName === 'string' || $propertyTypeName === 'mixed') {
            return new OAStringProperty();
        }

        if ($propertyTypeName === 'int') {
            return new OAIntegerProperty();
        }

        if ($propertyTypeName === 'float') {
            return new OANumberProperty();
        }

        if ($propertyTypeName === 'bool') {
            return new OABooleanProperty();
        }


        if (class_exists($propertyTypeName)) {
            if ((new \ReflectionClass($propertyTypeName))->isEnum()) {
                $property = new OAEnumProperty();
                $property->enum = $propertyTypeName::values();

                return $property;
            }

            if (RouterParser::classIsResource($propertyTypeName)) {
                return (new self($propertyTypeName))->getScheme();
            }
        }

        throw new \RuntimeException('Type of field not detected for type ' . $propertyTypeName);
    }
}
