<?php

namespace OpenApi\Converters\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\RequiredIf;
use OpenApi\Converters\Request\Rules\BaseOARuleConverter;
use OpenApi\Converters\Request\Rules\OAEnumRuleConverter;
use OpenApi\Converters\Request\Rules\OARuleAsStringConverter;
use OpenApi\Json\Properties\BaseOAProperty;
use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Properties\OAObjectProperty;
use OpenApi\Json\Properties\OAStringProperty;
use OpenApi\Json\Schemes\OAScheme;

class OARulesParser
{
    /** @var array<BaseOARuleConverter> */
    private array $converterClasses = [
        OAEnumRuleConverter::class,
        OARuleAsStringConverter::class,
    ];

    private static array $excludedForConvertingRules = [
        'exists',
        'bail',
        'prohibited_if',
        RequiredIf::class,
    ];

    public function __construct(private readonly FormRequest $request)
    {
    }

    public function getScheme(): OAScheme
    {
        $objects = [];
        $rules = collect($this->request->rules());
        $nestedRules = $rules->filter(fn($value, $key) => Str::contains($key, [".", '*']))->undot()->toArray();
        $scalarRules = $rules->filter(fn($value, $key) => !Str::contains($key, [".", '*']))->toArray();

        foreach ($scalarRules as $ruleKey => $rules) {
            if (is_string($rules)) {
                $rules = explode('|', $rules);
            }

            if (count($nestedRules[$ruleKey] ?? [])) {
                $objects[$ruleKey] = $this->parseNested($nestedRules[$ruleKey]);
            } else {
                $objects[$ruleKey] = $this->parseRules($ruleKey, $rules);
            }
        }

        return $this->prepareScheme($objects);
    }

    private function parseNested(array $nestedRules): OAObjectProperty|OAArrayProperty
    {
        if (count($nestedRules) && !isset($nestedRules['*'])) {
            $parent = new OAObjectProperty();
            $objects = [];
            foreach ($nestedRules as $ruleKey => $rules) {
                $objects[$ruleKey] = $this->parseRules($ruleKey, $rules);
            }
            $parent->properties = $objects;
        } else {
            $parent = new OAArrayProperty();

            $flattenCount = count(Arr::flatten($nestedRules['*']));
            $count = count($nestedRules['*']);

            if ($flattenCount === $count) {
                $objectRules = $this->parseRules('array', $nestedRules['*']);
                $parent->items = $objectRules;

                return $parent;
            }

            $object = $this->parseNested($nestedRules['*']);
            $parent->items = $object;
        }

        return $parent;
    }

    private function parseRules(string $fieldName, array $rules): BaseOAProperty
    {
        $object = null;

        $ruleNames = $this->getRuleNames($rules);

        foreach ($rules as $rule) {
            foreach ($this->converterClasses as $converterClass) {
                $object = $converterClass::detectObject($ruleNames, $rule);

                if ($object) {
                    break;
                }
            }

            if ($object) {
                break;
            }
        }

        if (!$object) {
            $object = new OAStringProperty();
        }

        foreach ($rules as $rule) {
            if ($this->isExcludedForConvertingRule($rule)) {
                continue;
            }

            $filled = false;

            foreach ($this->converterClasses as $converterClass) {
                $filled = $converterClass::fill($object, $rule);

                if ($filled) {
                    break;
                }
            }

            if (!$filled) {
                $this->throwException("Not detected parser for field '$fieldName'", $rule);
            }
        }

        return $object;
    }

    private function getRuleNames(array $rules): array
    {
        $ruleNames = [];

        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }

            $ruleNames[] = explode(':', $rule)[0];
        }

        return $ruleNames;
    }

    private function throwException(string $message, $data = null)
    {
        throw new \RuntimeException(
            "Request: " . get_class($this->request) . '. ' . $message . ($data ? (PHP_EOL . print_r($data, true)) : '')
        );
    }

    private function isExcludedForConvertingRule(mixed $rule): bool
    {
        if (is_callable($rule)) {
            return true;
        }

        if (is_object($rule)) {
            $rule = get_class($rule);
        }

        $convertingRules = array_merge(self::$excludedForConvertingRules, config('open-api.excluded_rules'));

        return in_array($rule, $convertingRules);
    }

    /**
     * @param array<string, BaseOAProperty> $properties
     */
    private function prepareScheme(array $properties): OAScheme
    {
        $oneOf = [];

        $requiredWithoutFieldNames = [];

        foreach ($properties as $fieldName => $object) {
            if (!$object->requiredWithout) {
                continue;
            }

            $requiredWithoutFieldNames[] = $fieldName;

            $schema = new OAScheme();
            $schema->properties[$fieldName] = $object;
            $schema->required = $object->requiredWithout;

            $oneOf[] = $schema;
        }

        $schema = new OAScheme();

        if ($oneOf) {
            $schema->oneOf = $oneOf;
        }

        $filteredProperties = collect($properties)
            ->filter(function (BaseOAProperty $property, string $fieldName) use ($requiredWithoutFieldNames) {
                return !in_array($fieldName, $requiredWithoutFieldNames);
            });

        $schema->properties = $filteredProperties->toArray();

        $schema->required = $filteredProperties
            ->filter(function (BaseOAProperty $property) {
                return $property->required;
            })
            ->keys()
            ->toArray();

        return $schema;
    }
}
