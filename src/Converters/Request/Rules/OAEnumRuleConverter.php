<?php

namespace OpenApi\Converters\Request\Rules;


use OpenApi\Enums\EnumRule;
use OpenApi\Json\Properties\BaseOAProperty;
use OpenApi\Json\Properties\OAEnumProperty;

class OAEnumRuleConverter extends BaseOARuleConverter
{
    protected static function canHandleRule(mixed $rule): bool
    {
        return is_object($rule) && get_class($rule) === EnumRule::class;
    }

    protected static function onDetectObject(array $ruleNames, mixed $rule): ?BaseOAProperty
    {
        return new OAEnumProperty();
    }

    protected static function onFill(BaseOAProperty $object, mixed $rule)
    {
        /**
         * @var OAEnumProperty $object
         * @var EnumRule $rule
         */

        $object->enum = $rule->getType()::values();
    }
}