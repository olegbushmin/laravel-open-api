<?php

namespace OpenApi\Converters\Request\Rules;

use Illuminate\Support\Str;
use OpenApi\Json\Properties\BaseOAProperty;
use OpenApi\Json\Properties\OAArrayProperty;
use OpenApi\Json\Properties\OABooleanProperty;
use OpenApi\Json\Properties\OAIntegerProperty;
use OpenApi\Json\Properties\OANumberProperty;
use OpenApi\Json\Properties\OAStringProperty;

class OARuleAsStringConverter extends BaseOARuleConverter
{
    private static array $excludedForFillRules = [
        'string',
        'integer',
        'number',
        'int',
        'numeric',
        'array',
        'mimetypes',
        'dimensions',
        'unique',
        'exists',
        'ipv4',
        'ipv6',
        'gt', // TODO: need to implement.. maybe
        'different', // TODO: need to implement.. maybe
        'prohibited_if',
        'after',
    ];

    protected static function canHandleRule(mixed $rule): bool
    {
        return is_string($rule);
    }

    public static function onDetectObject(array $ruleNames, mixed $rule): ?BaseOAProperty
    {
        $isFile     = in_array('file', $ruleNames);
        $isEmail    = in_array('email', $ruleNames);
        $isPassword = in_array('password', $ruleNames);
        $isIpv4     = in_array('ipv4', $ruleNames);
        $isIpv6     = in_array('ipv6', $ruleNames);
        $isDate     = in_array('date', $ruleNames);
        $isMimetype = in_array('mimetypes', $ruleNames);

        if ($isFile
            || $isEmail
            || $isPassword
            || $isIpv4
            || $isIpv6
            || $isDate
            || $isMimetype
            || in_array('string', $ruleNames)
        ) {
            $object = new OAStringProperty();

            if ($isFile) {
                $object->format = OAStringProperty::FORMAT_BINARY;
            } elseif ($isEmail) {
                $object->format = OAStringProperty::FORMAT_EMAIL;
            } elseif ($isPassword) {
                $object->format = OAStringProperty::FORMAT_PASSWORD;
            } elseif ($isIpv4) {
                $object->format = OAStringProperty::FORMAT_IPV4;
            } elseif ($isIpv6) {
                $object->format = OAStringProperty::FORMAT_IPV6;
            } elseif ($isDate) {
                $object->format = OAStringProperty::FORMAT_DATE;
            } elseif ($isMimetype) {
                $object->format = OAStringProperty::FORMAT_BINARY;
            }

            return $object;
        }

        if (in_array('int', $ruleNames) || in_array('integer', $ruleNames)) {
            return new OAIntegerProperty();
        }

        if (in_array('numeric', $ruleNames)) {
            $object         = new OANumberProperty();
            $object->format = OANumberProperty::FORMAT_FLOAT;

            return $object;
        }

        if (in_array('array', $ruleNames)) {
            return new OAArrayProperty();
        }

        if (in_array('bool', $ruleNames) || in_array('boolean', $ruleNames)) {
            return new OABooleanProperty();
        }

        return null;
    }

    protected static function onFill(BaseOAProperty $object, mixed $rule)
    {
        $ruleData = explode(':', $rule);

        $ruleName = strtolower($ruleData[0]);

        if (in_array($ruleName, self::$excludedForFillRules)) {
            return;
        }

        $parameters = $ruleData[1] ?? null;

        if ($ruleName === 'required') {
            $object->required = true;

            return;
        }

        if ($ruleName === 'sometimes') {
            $object->required = false;

            return;
        }

        if ($ruleName === 'nullable') {
            $object->nullable = true;

            return;
        }

        if ($ruleName === 'email') {
            $object->format = OAStringProperty::FORMAT_EMAIL;

            return;
        }

        if ($ruleName === 'min') {
            switch (get_class($object)) {
                case OAStringProperty::class:
                    $object->minLength = $parameters;
                    break;
                case OANumberProperty::class:
                case OAIntegerProperty::class:
                    $object->minimum = $parameters;
                    break;
                case OAArrayProperty::class:
                    $object->minItems = $parameters;
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        if ($ruleName === 'max') {
            switch (get_class($object)) {
                case OAStringProperty::class:
                    $object->maxLength = $parameters;
                    break;
                case OANumberProperty::class:
                case OAIntegerProperty::class:
                    $object->maximum = $parameters;
                    break;
                case OAArrayProperty::class:
                    $object->maxItems = $parameters;
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        if ($ruleName === 'numeric') {
            switch (get_class($object)) {
                case OANumberProperty::class:
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        if ($ruleName === 'digits') {
            switch (get_class($object)) {
                case OANumberProperty::class:
                case OAIntegerProperty::class:
                    $object->minimum = Str::of('1')->padRight($parameters, '0')->toInteger();
                    $object->maximum = Str::of('')->padRight($parameters, '9')->toInteger();
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        if ($ruleName === 'gt') {
            switch (get_class($object)) {
                case OANumberProperty::class:
                    $object->greater = $parameters;
                    break;
                case OAIntegerProperty::class:
                    $object->greater = $parameters;
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        if ($ruleName === 'present') {
            $object->required = false;
            $object->nullable = false;

            return;
        }

        if (in_array($ruleName, ['required_if', 'required_without', 'required_with', 'required_without_all'])) {
            $object->requiredWithout = explode(',', $parameters);

            return;
        }

        if ($ruleName === 'bool' || $ruleName === 'boolean') {
            return;
        }

        if ($ruleName === 'in') {
            switch (get_class($object)) {
                case OAStringProperty::class:
                case OAIntegerProperty::class:
                    $object->enum = explode(',', $parameters);
                    break;
                default:
                    throw new \RuntimeException(
                        "Object '" . get_class($object) . "' can't to keep '$ruleName' rule"
                    );
            }

            return;
        }

        throw new \RuntimeException("Not implemented rule: " . $rule);
    }
}
