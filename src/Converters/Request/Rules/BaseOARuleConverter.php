<?php

namespace OpenApi\Converters\Request\Rules;

use OpenApi\Json\Properties\BaseOAProperty;

abstract class BaseOARuleConverter
{
    abstract protected static function canHandleRule(mixed $rule): bool;
    abstract protected static function onDetectObject(array $ruleNames, mixed $rule): ?BaseOAProperty;
    abstract protected static function onFill(BaseOAProperty $object, mixed $rule);

    public static function fill(BaseOAProperty $object, mixed $rule): bool
    {
        if (!static::canHandleRule($rule)) {
            return false;
        }

        static::onFill($object, $rule);

        return true;
    }

    public static function detectObject(array $ruleNames, mixed $rule): ?BaseOAProperty
    {
        if (!static::canHandleRule($rule)) {
            return null;
        }

        return static::onDetectObject($ruleNames, $rule);
    }
}