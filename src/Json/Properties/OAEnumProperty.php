<?php

namespace OpenApi\Json\Properties;

use OpenApi\Json\OATypeEnum;

class OAEnumProperty extends BaseOAProperty
{
    /** @var array<string> */
    public array $enum = [];

    // STUB
    protected function getType(): OATypeEnum
    {
        return OATypeEnum::String;
    }

    protected function propertiesToArray(): array
    {
        if (!$this->enum) {
            throw new \RuntimeException("Property 'enum' is empty");
        }

        $enumItemsType = gettype($this->enum[0]);

        $type = match ($enumItemsType) {
            'integer' => OATypeEnum::Integer,
            'string' => OATypeEnum::String,
            default => throw new \RuntimeException("Type '$enumItemsType' not implemented!"),
        };

        $data = [];

        $this->addToArrayIfNotNull($data, 'type', $type);
        $this->addToArrayIfNotNull($data, 'enum', $this->enum);

        return $data;
    }
}
