<?php

namespace OpenApi\Json\Properties;


use OpenApi\Json\OATypeEnum;

class OAObjectProperty extends BaseOAProperty
{
    public ?int $minProperties = null;
    public ?int $maxProperties = null;

    /** @var array<string, BaseOAProperty>|null */
    public ?array $properties = null;

    /** @var array<string>|null */
    public ?array $requiredProperties = null;

    protected function getType(): OATypeEnum
    {
        return OATypeEnum::Object;
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        $this->addToArrayIfNotNull($data, 'properties', $this->properties);
        $this->addToArrayIfNotNull($data, 'minProperties', $this->minProperties);
        $this->addToArrayIfNotNull($data, 'maxProperties', $this->maxProperties);
        $this->addToArrayIfNotNull($data, 'required', $this->requiredProperties);

        return $data;
    }
}