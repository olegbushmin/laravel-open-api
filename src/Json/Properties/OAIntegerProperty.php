<?php

namespace OpenApi\Json\Properties;

use OpenApi\Json\OATypeEnum;

class OAIntegerProperty extends BaseOAProperty
{
    const FORMAT_INT32 = 'int32';
    const FORMAT_INT64 = 'int64';

    public ?int  $minimum          = null;
    public ?int  $maximum          = null;
    public ?int  $multipleOf       = null;
    public ?bool $exclusiveMinimum = null;
    public ?bool $exclusiveMaximum = null;
    public ?int  $greater          = null;
    public ?int  $less             = null;

    protected function getType(): OATypeEnum
    {
        return OATypeEnum::Integer;
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        $this->addToArrayIfNotNull($data, 'minimum', $this->minimum);
        $this->addToArrayIfNotNull($data, 'maximum', $this->maximum);
        $this->addToArrayIfNotNull($data, 'multipleOf', $this->multipleOf);
        $this->addToArrayIfNotNull($data, 'exclusiveMinimum', $this->exclusiveMinimum);
        $this->addToArrayIfNotNull($data, 'exclusiveMaximum', $this->exclusiveMaximum);

        return $data;
    }
}
