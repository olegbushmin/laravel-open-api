<?php

namespace OpenApi\Json\Properties;

use OpenApi\Json\BaseOAJsonObject;
use OpenApi\Json\OATypeEnum;

class OAArrayProperty extends BaseOAProperty
{
    public ?int  $minItems    = null;
    public ?int  $maxItems    = null;
    public ?bool $uniqueItems = null;
    public bool $isPagination = false;

    public ?BaseOAJsonObject $items = null;

    protected function getType(): OATypeEnum
    {
        return OATypeEnum::Array;
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        $this->addToArrayIfNotNull($data, 'minItems', $this->minItems);
        $this->addToArrayIfNotNull($data, 'maxItems', $this->maxItems);
        $this->addToArrayIfNotNull($data, 'uniqueItems', $this->uniqueItems);
        $this->addToArrayIfNotNull($data, 'items', $this->items);

        return $data;
    }
}
