<?php

namespace OpenApi\Json\Properties;

use OpenApi\Json\OATypeEnum;

class OAStringProperty extends BaseOAProperty
{
    const FORMAT_BYTE     = 'byte';
    const FORMAT_BINARY   = 'binary';
    const FORMAT_DATE     = 'date';
    const FORMAT_DATETIME = 'date-time';
    const FORMAT_PASSWORD = 'password';
    const FORMAT_EMAIL    = 'email';
    const FORMAT_PHONE    = 'phone';
    const FORMAT_UUID     = 'uuid';
    const FORMAT_URI      = 'uri';
    const FORMAT_HOSTNAME = 'hostname';
    const FORMAT_IPV4     = 'ipv4';
    const FORMAT_IPV6     = 'ipv6';

    public ?int $maxLength = null;
    public ?int $minLength = null;
    public ?string $pattern = null;
    public ?array $enum = null;

    protected function getType(): OATypeEnum
    {
        return OATypeEnum::String;
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        $this->addToArrayIfNotNull($data, 'maxLength', $this->maxLength);
        $this->addToArrayIfNotNull($data, 'minLength', $this->minLength);
        $this->addToArrayIfNotNull($data, 'pattern', $this->pattern);
        $this->addToArrayIfNotNull($data, 'enum', $this->enum);

        return $data;
    }
}
