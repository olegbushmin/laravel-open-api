<?php

namespace OpenApi\Json\Properties;

class OAPaginationInfoProperty extends OAObjectProperty
{
    public function __construct()
    {
        $this->properties = [
            'current_page' => new OANumberProperty(),
            'last_page' => new OANumberProperty(),
        ];

        $this->requiredProperties = [
            'current_page',
            'last_page',
        ];
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        $this->addToArrayIfNotNull($data, 'properties', $this->properties);
        $this->addToArrayIfNotNull($data, 'required', $this->requiredProperties);

        return $data;
    }
}
