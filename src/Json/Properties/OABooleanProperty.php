<?php

namespace OpenApi\Json\Properties;

use OpenApi\Json\OATypeEnum;

class OABooleanProperty extends BaseOAProperty
{
    protected function getType(): OATypeEnum
    {
        return OATypeEnum::Boolean;
    }

    protected function propertiesToArray(): array
    {
        return [];
    }
}