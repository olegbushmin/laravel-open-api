<?php

namespace OpenApi\Json\Schemes;

use OpenApi\Json\BaseOAJsonObject;
use OpenApi\Json\OATypeEnum;
use OpenApi\Json\Properties\BaseOAProperty;

class OAScheme extends BaseOAJsonObject
{
    /** @var array<string, BaseOAProperty> */
    public array $properties = [];

    /** @var array<string> */
    public array $required = [];

    /** @var array<OAScheme> */
    public array $oneOf = [];

    protected function getType(): OATypeEnum
    {
        return OATypeEnum::Object;
    }

    protected function basePropertiesToArray(): array
    {
        return $this->propertiesToArray();
    }

    protected function propertiesToArray(): array
    {
        $data = [];

        if ($this->properties) {
            $data['properties'] = $this->properties;
        }

        if ($this->required) {
            $data['required'] = $this->required;
        }

        if ($this->oneOf) {
            $data['oneOf'] = $this->oneOf;
        }

        return $data;
    }
}
