<?php

namespace OpenApi;

use Illuminate\Support\ServiceProvider;
use OpenApi\Console\Commands\OpenApiGenerateCommand;

class OpenApiServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/open-api.php' => config_path('open-api.php'),
        ]);

        $this->commands([
            OpenApiGenerateCommand::class,
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/open-api.php', 'open-api'
        );

        $this->app->bind(OpenApiService::class);
    }
}