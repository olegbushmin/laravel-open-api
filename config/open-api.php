<?php

return [
    'title' => env('OPEN_API_TITLE', ''),
    'base_resource' => env('BASE_RESOURCE_PATH'),
    'base_request' => env('BASE_REQUEST_PATH'),
    'excluded_rules' => []
];